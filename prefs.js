
const Gtk = imports.gi.Gtk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Utils = Me.imports.utils;
const Soup = imports.gi.Soup;
const Lang = imports.lang;

let httpSession = new Soup.SessionAsync();
Soup.Session.prototype.add_feature.call(httpSession, new Soup.ProxyResolverDefault());

const Convenience = Me.imports.convenience;
const Gettext = imports.gettext.domain('uulmMensa');
//const _ = Gettext.gettext;

let settings;

function init() {
    settings = Utils.getSettings(Me);
    Convenience.initTranslations("uulmMensa");
}

function buildPrefsWidget() {

    // Prepare labels and controls
    let buildable = new Gtk.Builder();
    buildable.add_from_file(Me.dir.get_path() + '/Settings.ui');
    let box = buildable.get_object('settings_widget');

    buildable.get_object('extension_name_label').set_text(Me.metadata.name.toString() + " Extension");
    buildable.get_object('extension_version_number').set_text(Me.metadata.version.toString());
    
    let isOpenSwitch = buildable.get_object('isopen_switch');
    let priceComboBox = buildable.get_object('price_combo');
    let refreshComboBox = buildable.get_object('refresh_combo');

    // indicator settings
    settings.bind('show-open', isOpenSwitch, 'active', Gio.SettingsBindFlags.DEFAULT);

    // price settings
    settings.bind('price-category', priceComboBox, 'active', Gio.SettingsBindFlags.DEFAULT);

    return box;
};
