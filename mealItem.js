const Lang = imports.lang;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const Util = imports.misc.util;
const ExtensionUtils = imports.misc.extensionUtils;
const GObject = imports.gi.GObject;

var MealItem  = GObject.registerClass(
class MealItem extends PopupMenu.PopupBaseMenuItem {
    _init(mealType, mealName, mealPrice) {
        super._init()

        this.typeLbl = new St.Label({ text: mealType, x_expand: false });
        
        let vbox = new St.BoxLayout({ x_align: St.Align.MIDDLE, vertical: true});
        let hbox = new St.BoxLayout({width: 600});
        //let hbox2 = new St.BoxLayout({ x_align: St.Align.END});
        this.mealLbl = new St.Label({ text: mealName, x_expand: false, width: 500 });
        this.priceLbl = new St.Label({ text: mealPrice});

        this.placeHolderLbl = new St.Label({ text: "", x_expand: true});

        //hbox.add_child(this.MealLbl);
        //hbox.add_child(this.priceLbl);
        vbox.add_child(this.typeLbl);
        //this.add_child(new PopupMenu.PopupSeparatorMenuItem());
        
        hbox.add_child(this.mealLbl);
        //hbox2.add(this.priceLbl);
        hbox.add_child(this.placeHolderLbl);
        hbox.add(this.priceLbl);
        vbox.add_child(hbox);
        this.add_child(vbox);
        
        

        this.connect('activate', Lang.bind(this, function(event) {
           //connect to the web interface
        }));
    }
});
