const St = imports.gi.St;
const Main = imports.ui.main;
const Soup = imports.gi.Soup;
const Lang = imports.lang;
const Gio = imports.gi.Gio;
const Util = imports.misc.util;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Shell = imports.gi.Shell;
const GObject = imports.gi.GObject;

imports.gi.versions.Gdk = '3.0';
imports.gi.versions.Gtk = '3.0';

const Gtk = imports.gi.Gtk;
const Gdk = imports.gi.Gdk;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Utils = Me.imports.utils;

const Convenience = Me.imports.convenience;

//The information about the different canteens menues are saved in two different JSON files
const mensaRootURL = "https://www.uni-ulm.de/mensaplan/";
const mensaPlanURLS = [mensaRootURL + "data/mensaplan.json", mensaRootURL + "data/mensaplan_static.json"];
const IndicatorName = "uulmMensaIndicator";
const ICON = "icon";

var MealItem = Me.imports.mealItem.MealItem;

let uulmMensaIndicator = null;
let httpSession = new Soup.SessionAsync();
Soup.Session.prototype.add_feature.call(httpSession, new Soup.ProxyResolverDefault());


//the following methods are for outputing debug/error messages
function log(msg) {
    if (uulmMensaIndicator == null)
        print("uulmMensa extension: " + msg);
}

function notifyError(msg) {
    Main.notifyError("uulmMensa extension error", msg);
}

function notifyMsg(msg) {
    Main.notifyError("uulmMensa extension", msg);
}


//This is the component which is used to display the Mensa-Menu of a specific canteen
const MensaSubMenu = GObject.registerClass(
    class MensaSubMenu extends PopupMenu.PopupSubMenuMenuItem {
        _init(mensaName, openStatus, mealsList) {
            super._init("loading...", false);
            this.mensaName = mensaName;
            this.openStatus = openStatus;
            this.openStatus = "";

            //show the current opening status of the canteen
            if (uulmMensaIndicator._settings.get_boolean('show-open')) {
                if (openStatus) {
                    this.openStatus = "🔹 ";
                }
                else {
                    this.openStatus = "🔸 ";
                }
            }

            //update the text in the menu header
            this.label.set_text(translateMensaName(this.openStatus + this.mensaName));
            this.updateMensaList(mealsList);
        }

        updateMensaList(mealsList) {

            if (mealsList === undefined) {
                newMensaList = ["cound not read Mensa list"];
                return;
            }

            var mealsMenu = this.menu;

            Object.keys(mealsList).forEach(Lang.bind(this, function (k) {
                var currentObject = mealsList[k];
                if (currentObject.category != undefined) {
                    var price = "";
                    if (currentObject.price != undefined) {
                        price = priceParse(currentObject.price);
                    }
                    let item = new MealItem(currentObject.category, currentObject.meal, price);
                    item.connect('button-press-event', Lang.bind(this, function () {
                        Util.spawn(["xdg-open", (mensaRootURL + "#" + this.mensaName)]);
                    }));
                    mealsMenu.addMenuItem(item);
                }
            }));
        }
    }
);




const UulmMensaIndicator = new Lang.Class({
    Name: IndicatorName,
    Extends: PanelMenu.Button,

    _init() {
        this.parent(0.0, IndicatorName);

        this._updatesPending = 0;

        let gicon = Gio.icon_new_for_string(Me.dir.get_child('icons').get_path() + "/" + ICON + ".svg");
        this.icon = new St.Icon({ gicon: gicon, style_class: 'system-status-icon' });
        this.actor.add_child(this.icon);

        this._settings = Utils.getSettings();
        this._settings.connect('changed::hide', Lang.bind(this, function () {
        }));

        this.settingsItem = new PopupMenu.PopupMenuItem(_("Settings"));
        this.settingsItem.width = 660;
        this.mensaSection = new PopupMenu.PopupMenuSection();
        this.mensaSeperatorItem = new PopupMenu.PopupSeparatorMenuItem(" ");

        this.settingsItem.connect('activate', function () {
            Util.spawn(["gnome-shell-extension-prefs", Me.metadata.uuid]);
        });

        this.menu.connect('open-state-changed', Lang.bind(this, function(self, open){
            //only update value on menu open
            if(open){
                this._refresh();
            }
        }));

        this.menu.addMenuItem(this.mensaSection);
        this.menu.addMenuItem(this.mensaSeperatorItem);
        this.menu.addMenuItem(this.settingsItem);
        
    },

    _requestData(url) {
        let request = Soup.Message.new('GET', url);

        // queue the http request
        httpSession.queue_message(request, Lang.bind(this, function (httpSession, message) {
            if (message.status_code == 200) {
                let data = message.response_body.data;
                log("Recieved " + data.length + " bytes");
                this._parseData(data);
            } else if (message.status_code == 403) {
                log("Access denied: " + message.status_code);
                notifyMsg("Access to the server was denied");
                this._restartTimeout(TIMEOUT_SECONDS_ON_HTTP_ERROR);
            } else {
                log("Network error occured: " + message.status_code);
                notifyMsg("A network error occurred");
                this._restartTimeout(TIMEOUT_SECONDS_ON_HTTP_ERROR);
            }
            this._updatesPending -= 1;
        }));
    },

    _refresh() {

        //if an update process is alread running it will not start a new one
        if (this._updatesPending > 0) {
            //notifyMsg("update in progress");
            return;
        }
        //remove all Elements so items are not in the list twice
        this._removeElements();

        //check the different URLs for data
        for (i = 0; i < mensaPlanURLS.length; i++) {
            this._updatesPending += 1;
            this._requestData(mensaPlanURLS[i]);
        }
    },

    _removeElements() {
        this.mensaSection.removeAll();
    },

    _parseData(data) {
        let parsed = JSON.parse(data);
        var mensaSection = this.mensaSection;
        if (parsed != '') {
            //european format starts with monday. Ignoring edge case -1 because there is no menu on the weekend
            var dayNumber = new Date().getDay() - 1;
            var todayJSON = parsed.weeks["0"].days["" + dayNumber]
            Object.keys(todayJSON).forEach(function (k) {
                var currentObject = todayJSON[k];
                if (currentObject.meals != undefined) {
                    mensaSection.addMenuItem(new MensaSubMenu(k, currentObject.open, currentObject.meals));
                }
            });
        }
        else {
            //if no data is returned display an error message
            var noDataItem = new PopupMenu.PopupMenuItem(_("no data available 😕"))
            mensaSection.addMenuItem(noDataItem);
        }
    },

    stop() {
        if (this._timeout)
            Mainloop.source_remove(this._timeout);
        this._timeout = undefined;
        this.menu.removeAll();
    }
});

//because the prices are saved in strings of different format it is neccessary to
//extract them in this (arguably un-elegant) way
function priceParse(priceValue) {
    if ((priceValue != undefined) && (priceValue.includes("€"))) {
        if (!priceValue.includes("€ ")) {
            return priceValue;
        }
        matches = priceValue.match(/([0-9]+,[0-9][0-9])/g);
        if (matches != null) {
            return matches[uulmMensaIndicator._settings.get_int('price-category')] + "€";
        }
    }
    return "—";
}

//this is hardcoded for now. //TODO
function translateMensaName(mensaName) {
    if (mensaName == undefined) {
        return "unbekannte Mensa"
    }
    if (mensaName === "Mensa") {
        return "Mensa Uni-Süd"
    }
    if (mensaName === "Burgerbar") {
        return "Burgerbar Cafeteria SouthSide"
    }
    if (mensaName === "Bistro") {
        return "Bistro"
    }
    if (mensaName === "CB") {
        return "Cafetaria B"
    }
    if (mensaName === "West") {
        return "Cafetaria West"
    }
    if (mensaName === "Prittwitzstr") {
        return "Mensa Hochschule"
    }
    if (mensaName === "Diner") {
        return "WestSideDiner"
    }
    return mensaName;
}

function init(extensionMeta) {
}

function enable() {
    log("enable() called");
    uulmMensaIndicator = new UulmMensaIndicator();
    Main.panel.addToStatusArea(IndicatorName, uulmMensaIndicator);
}

function disable() {
    log("disable() called");
    if (this._timeout)
        Mainloop.source_remove(this._timeout);
    uulmMensaIndicator.stop();
    uulmMensaIndicator.destroy();
    uulmMensaIndicator = null;
}
